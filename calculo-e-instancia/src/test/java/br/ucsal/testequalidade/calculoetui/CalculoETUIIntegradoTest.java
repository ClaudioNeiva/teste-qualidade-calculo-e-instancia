package br.ucsal.testequalidade.calculoetui;

public class CalculoETUIIntegradoTest {

	// Dublê para o scanner (ByteArrayInputStream) -> aqui entrada de dados
	// Dublê para o System.out (ByteArrayOutputStream) -> outDuble.toString() ->
	// aqui saída atual

	// Entrada = 2
	// Saída esperada =
	// Informe um número maior ou igual a zero:\n
	// E(2)=2.5\n

	// Saída atual, com o erro no fatorial:
	// Informe um número maior ou igual a zero:\n
	// E(2)=3.0\n

}
